var module = angular.module('myApp', []);
module.directive('counter', ['$http', function ($http) {
    return {
        scope: {},
        controller: function ($scope, $http) {
            $scope.TOTAL_TEAM_MEMBERS = 5;
            $scope.DEADLINE = new Date(2015,10,16);
            $scope.now = new Date();
            $scope.workingDaysBetweenDates = calculateDays;
            var XML = parseXml(XML_RAW);
            var issueCount = XML.getElementsByTagName("issue").length;
            var sPEstimates = XML.getElementsByName("SP Estimate");
            $scope.totalIssues = sPEstimates.length;
            $scope.totalEstimates = 0;
            $scope.perTeamMember = 0;
            for (var i = 0; i < sPEstimates.length; i++) {
                $scope.totalEstimates += sPEstimates.item(i).firstChild.firstChild.textContent * 1;
            }
            $scope.perTeamMember = +($scope.totalEstimates / $scope.TOTAL_TEAM_MEMBERS).toFixed(1);
            $scope.hoursToWorkPerDay = $scope.perTeamMember / $scope.workingDaysBetweenDates($scope.now, $scope.DEADLINE);
        },
        templateUrl: 'main.html'
    };
}]);