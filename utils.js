function calculateDays( d0, d1 )
{
    var ndays = 1 + (d1.getTime()-d0.getTime())/(24*3600*1000);
    var nsaturdays = Math.floor((ndays + d0.getDay()) / 7);
    return (ndays - 2*nsaturdays + (d0.getDay()==6) - (d1.getDay()==5));
}